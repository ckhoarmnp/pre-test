﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckExercise : MonoBehaviour {
  public static int ME = 0xFACE;

  [SerializeField]
  private Exercise_1 Exercise_1;
  [SerializeField]
  private Exercise_2 Exercise_2;
  [SerializeField]
  private Exercise_3 Exercise_3;
  [SerializeField]
  private Exercise_4 Exercise_4;
  [SerializeField]
  private Exercise_5 Exercise_5;
  [SerializeField]
  private Exercise_6 Exercise_6;
  [SerializeField]
  private Exercise_7 Exercise_7;

  [SerializeField]
  private GameObject[] Mark;

  void Start()
  {
    Mark[0].SetActive(Exercise_1.Run());
    Mark[1].SetActive(Exercise_2.Run());
    Mark[2].SetActive(true);
    for (int i = 0; i < 3; ++i)
    {
      if (!Exercise_3.Run(Random.Range(1,21)))
      {
        Mark[2].SetActive(false);
      }
    }
    Mark[3].SetActive(Exercise_4.Run());
    int[] x = new int[] { 8, 6, 9, 7, 5, 4, 2, 1, 3 };
    Mark[4].SetActive(Exercise_5.Run(x));
    Mark[5].SetActive(Exercise_6.Run());
    Mark[6].SetActive(Exercise_7.Run(Exercise_3.gameObject, Exercise_1.gameObject));
  }
}
