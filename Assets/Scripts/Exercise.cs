﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exercise : MonoBehaviour {

  public bool Run(float answer, int exercise)
  {
    if (exercise == 1)
    {
      if (answer == 10) return true;
      else return false;
    }
    else if (exercise == 4)
    {
      if (answer == 39916800) return true;
      else return false;
    }
    else if (exercise == 6)
    {
      if (answer == 0xFACE) return true;
      else return false;
    }
    else
    {
      if (answer == 1) return true;
      else return false;
    }
  }

  public bool Run(float answer,int testCase, int exercise)
  {
    if (testCase > 5 && testCase < 10)
    {
      if (answer == 10) return true;
      else return false;
    }
    else if (testCase < 5)
    {
      if (answer == 1) return true;
      else return false;
    }
    else if (testCase >= 10)
    {
      if (answer == 999) return true;
      else return false;
    }
    return false;
  }

  public bool Run(string answer, int exercise)
  {
    if (answer == "Hello world") return true;
    else return false;
  }

  public bool Run(int[] answer, int exercise)
  {
    for (int i = 0; i < answer.Length; ++i)
    {
      if (answer[i] != i + 1) return false;
    }
    return true;
  }
}
